import React from 'react';
import Link from './link';

const Footer = () => {
  return (
    <div id="footer">
      <div className="container">
        <p>Using <Link to="https://uptimerobot.com/" text="UptimeRobot" /> through <Link to="https://github.com/yb/uptime-status" text="uptime-status"/></p>
      </div>
    </div>
  );
}

export default Footer;

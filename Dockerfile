FROM node

WORKDIR /root/builder

COPY package.json .

RUN mkdir node_modules && npm install

WORKDIR /root/build

CMD cp -r /root/builder/node_modules /root/build/ \
  && npm run build
